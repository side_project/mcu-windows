import { defineConfig, presetUno } from 'unocss'

export default defineConfig({
  shortcuts: [
    {
      'fit': 'w-full h-full',
      'flex-col': 'flex flex-col',
      'flex-center': 'flex justify-center items-center',
      'border': 'border border-solid border-[#dedede]',
      'border-b': 'border-b border-b-solid border-[#dedede]',
    },
    [/^btn-(.*)$/, ([, c]) => `bg-${c}-400 text-${c}-100 py-2 px-4 rounded-lg`],
  ],
  rules: [],
  presets: [
    presetUno(),
  ],
  autocomplete: {
    templates: ['gradient-effect']
  }
})