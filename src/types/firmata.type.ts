import type { ValueOf } from 'type-fest';

export enum PinMode {
  /** 數位輸入 : 0x00 */
  DIGITAL_INPUT = 0x00,
  /** 數位輸出 : 0x01 */
  DIGITAL_OUTPUT = 0x01,
  /** 類比輸入 : 0x02 */
  ANALOG_INPUT = 0x02,
  PWM = 0x03,
  SERVO = 0x04,
  SHIFT = 0x05,
  I2C = 0x06,
  STEPPER = 0x08,
  ONEWIRE = 0x07,
  ENCODER = 0x09,
  SERIAL = 0x0A,
  /** 數位上拉輸入 : 0x0B */
  INPUT_PULLUP = 0x0B,
  SPI = 0x0C,
  SONAR = 0x0D,
  TONE = 0x0E,
  DHT = 0x0F,
}

export type PinModeKey = keyof typeof PinMode;

export interface PinModeDefinition {
  code: typeof PinMode[keyof typeof PinMode],
  key: PinModeKey,
  name: string,
  color: string,
}

export const pinModeDefinitions: PinModeDefinition[] = [
  {
    code: 0x00,
    key: 'DIGITAL_INPUT',
    name: 'Digital Input',
    color: 'light-blue-3',
  },
  {
    code: 0x01,
    key: 'DIGITAL_OUTPUT',
    name: 'Digital Output',
    color: 'cyan-3',
  },
  {
    code: 0x02,
    key: 'ANALOG_INPUT',
    name: 'Analog Input',
    color: 'red-4',
  },
  {
    code: 0x03,
    key: 'PWM',
    name: 'PWM',
    color: 'light-green-4',
  },
  {
    code: 0x04,
    key: 'SERVO',
    name: 'Servo',
    color: 'blue-5',
  },
  {
    code: 0x05,
    key: 'SHIFT',
    name: 'Shift',
    color: 'purple-3',
  },
  {
    code: 0x06,
    key: 'I2C',
    name: 'I2C',
    color: 'green-4',
  },
  {
    code: 0x07,
    key: 'ONEWIRE',
    name: 'Onewire',
    color: 'indigo-4',
  },
  {
    code: 0x08,
    key: 'STEPPER',
    name: 'Stepper',
    color: 'lime-4',
  },
  {
    code: 0x09,
    key: 'ENCODER',
    name: 'Encoder',
    color: 'yellow-4',
  },
  {
    code: 0x0A,
    key: 'SERIAL',
    name: 'Serial',
    color: 'amber-5',
  },

  {
    code: 0x0B,
    key: 'INPUT_PULLUP',
    name: 'Input Pullup',
    color: 'teal-3',
  },
  {
    code: 0x0C,
    key: 'SPI',
    name: 'SPI',
    color: 'amber-4',
  },
  {
    code: 0x0D,
    key: 'SONAR',
    name: 'Sonar',
    color: 'orange-4',
  },
  {
    code: 0x0E,
    key: 'TONE',
    name: 'Tone',
    color: 'deep-orange-4',
  },
  {
    code: 0x0F,
    key: 'DHT',
    name: 'DHT',
    color: 'brown-3',
  }
] as const;