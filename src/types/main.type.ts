import { JoyStick } from "../electronic-components/joy-stick";

export type JoyStickGame = Phaser.Game & {
  joyStick?: JoyStick;
};
