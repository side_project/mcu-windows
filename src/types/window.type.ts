import { defineComponent } from "vue";
import { Pin } from "../common/firmata/response-define";

/** 可用視窗名稱 */
export type WindowName =
  'digital-io' | 'analog-input' | 'pwm-output' |

  'rgb-led-palette' | 'morse-code-transmitter' |
  'thermo-hygrometer' |

  'google-dino' | 'cat-vs-dog';

export interface Window {
  /** 視窗實例 */
  component: ReturnType<typeof defineComponent>;
  name: WindowName;
  /** 視窗 ID：每個視窗的唯一 ID，用來識別視窗 */
  id: string;
  /** 聚焦時間：用來判斷視窗重疊關係 */
  focusAt: number;
  /** 占用腳位 */
  occupiedPins: Pin[];
}