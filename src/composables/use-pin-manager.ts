import { computed, ref } from 'vue'
import { PinMode } from '../types';
import { Pin } from '../common/firmata/response-define';
import { clone } from 'remeda';
import { useBoardStore } from '../stores/board.store';
import { useWindowStore } from '../stores/window.store';

export interface UseWindowParam {
  windowId: string;
}

/** 管理腳位 */
export function usePinManager({ windowId }: UseWindowParam) {
  const usedPins = ref<Pin[]>([]);
  const boardStore = useBoardStore();
  const windowStore = useWindowStore();

  return {
    usedPins: computed(() => clone(usedPins.value)),

    /** 根據功能產生腳位清單 */
    createPinList(capabilities: PinMode[]) {
      return computed(() =>
        boardStore.pins.filter((pin) =>
          pin.capabilities.some((capability) =>
            capabilities.includes(capability.mode)
          )
        )
      );
    },
    /** 保留腳位。會占用指定腳位 */
    reservePin(pin: Pin) {
      usedPins.value.push(pin);
      windowStore.addOccupiedPin(windowId, pin);
    },

    /** 釋放腳位。會停止占用指定腳位 */
    unreservePin(pin: Pin) {
      windowStore.deleteOccupiedPin(windowId, pin);

      const index = usedPins.value.findIndex((existPin) =>
        existPin.number === pin.number
      );
      usedPins.value.splice(index, 1);
    },
  }
}