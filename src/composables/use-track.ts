import { ref } from 'vue';
import { event } from 'vue-gtag'
import { useGtm } from '@gtm-support/vue-gtm';

export enum GaEvent {
  GET_FIRMWARE_INFO = 'get_firmware_info',

  ADD_WINDOW = 'add_window',
  REMOVE_WINDOW = 'remove_window',

  RESERVE_PIN = 'reserve_pin',
  UNRESERVE_PIN = 'unreserve_pin',
}

export enum GaCategory {
  FIRMWARE = 'firmware',
  VERSION = 'version',
  WINDOW = 'window',
  PIN = 'pin',
}

export interface EventParam {
  /** 事件名稱 */
  event: `${GaEvent}`,
  /** 事件分類 */
  category: `${GaCategory}`,
  /** 事件動作，click 等等 */
  action?: string,
  /** 事件細節 */
  label?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value?: any;
  /** 是否應計為與用戶互動 */
  noninteraction?: boolean;
}
export function useTrack() {
  const gtm = useGtm();

  return {
    event(param: EventParam) {
      gtm?.trackEvent(param);

      event(param.event, {
        event_category: param.category,
        event_label: param.label,
        value: param.value,
        non_interaction: param.noninteraction,
      });
    },
  }
}