import { chunk, compact, divide, equals, findLastIndex, hasAtLeast, join, map, pipe, reject, splitWhen } from "remeda";
import { arraySplit, matchFeature, significantBytesToNumber } from "../utils";

/** 事件名稱種類 */
export enum EventName {
  FIRMWARE_NAME = 'firmware-name',
  CAPABILITY = 'capability',
  ANALOG_MAPPING = 'analog-mapping',

  DIGITAL_DATA = 'digital-data',
  ANALOG_DATA = 'analog-data',
  DHT_DATA = 'dht-data',

  /** 來自 MCU 的訊息。通常用於錯誤訊息或除錯 */
  MESSAGE = 'message',
}

/** 回應基本定義 */
interface ResponseDefine<
  Event extends `${EventName}`, Data
> {
  /** 此資料對應觸發的 event 名稱 */
  eventName: Event;
  /** 用來判斷回應資料是否符合 */
  match: (res: number[]) => boolean;
  /** 從 byte 中取得資料 */
  getData: (res: number[]) => Data;
}

/** Firmata 資訊 */
export interface FirmataInfo {
  /** 版本號 */
  version: string;
  /** 韌體名稱 */
  firmwareName: string;
}
/** Firmata 腳位資料 */
export interface FirmataPins {
  /** 版本號 */
  pins: Pin[];
}
/** Firmata 類比腳位映射表 */
export interface FirmataAnalogPinMap {
  /** 版本號 */
  analogPinMap: AnalogPinMap;
}

/** 腳位 */
export interface Pin {
  /** 腳位編號 */
  number: number;
  /** 腳位能力 */
  capabilities: Capability[];
}
/** 功能 */
export interface Capability {
  /** 支援模式 */
  mode: number;
  /** 解析度 */
  resolution: number;
}
/** 類比腳位映射表 */
export interface AnalogPinMap {
  /** 原腳位編號對應映射後的編號 */
  [pinNumber: string]: number;
}
/** 數位訊號資料 */
export interface DigitalData {
  /** port 編號 */
  port: number;
  /** 以二進位形式儲存整個 Port 腳位狀態 */
  value: number;
}
/** 類比訊號資料 */
export interface AnalogData {
  /** 類比編號，實際腳位編號需要使用 AnalogPinMap 查詢 */
  analogNumber: number;
  /** ADC 轉換後的數值 */
  value: number;
}

/** DHT 感測器資料 */
export interface DhtData {
  /** 溫度。
   * - 範圍：-40~80℃
   * - 解析度：0.1℃
   * - 準確度：<0.5℃
   */
  temperature: number;
  /** 濕度
   * - 範圍：0-100%RH
   * - 解析度：0.1%RH
   * - 精度：+-2 %RH（最大+-5 %RH）
   */
  humidity: number;
}

export interface Message {
  text: string;
}

export type FirmataData = ReturnType<FirmataResponse['getData']>;

type ResponseFirmwareName = ResponseDefine<'firmware-name', FirmataInfo>;
type ResponseCapability = ResponseDefine<'capability', FirmataPins>;
type ResponseAnalogMapping = ResponseDefine<'analog-mapping', FirmataAnalogPinMap>;
type ResponseDigitalData = ResponseDefine<'digital-data', DigitalData[]>;
type ResponseAnalogData = ResponseDefine<'analog-data', AnalogData[]>;
type ResponseDhtData = ResponseDefine<'dht-data', DhtData[]>;
type ResponseMessage = ResponseDefine<'message', Message>;

export type FirmataResponse =
  ResponseFirmwareName | ResponseCapability | ResponseAnalogMapping |
  ResponseDigitalData | ResponseAnalogData |
  ResponseDhtData |
  ResponseMessage

/** 回應定義清單 */
export const responses: FirmataResponse[] = [
  // firmware-name: 韌體名稱與版本
  {
    eventName: 'firmware-name',
    match(res) {
      // 回應開頭一定為 F0 79 (240  121)
      const featureBytes = [0xF0, 0x79];
      return matchFeature(res, featureBytes);
    },

    getData(res) {
      // 取得特徵起點
      const index = res.indexOf(0x79);

      // 版本號
      const major = res[index + 1];
      const minor = res[index + 2];

      // 取得剩下的資料
      const nameBytes = res.slice(index + 3, -1);

      /** 由於 MSB 都是 0
       * 所以去除 0 之後，將剩下的 byte 都轉為字元後合併
       * 最後的結果就會是完整的名稱
       */
      const firmwareName = nameBytes
        .filter((byte) => byte !== 0)
        .map((byte) => String.fromCharCode(byte))
        .join('');

      return {
        version: `${major}.${minor}`,
        firmwareName
      }
    },
  },
  // capability: 腳位與功能
  {
    eventName: 'capability',
    match(res) {
      const featureBytes = [0xF0, 0x6C];
      return matchFeature(res, featureBytes);
    },

    getData(res) {
      // 去除起始值、命令代號、結束值
      const values = res.filter((byte) =>
        ![0xF0, 0x6C, 0xF7].includes(byte)
      );

      // 根據分隔符號 0x7F 分割
      const pinParts = arraySplit(values, 0x7F);

      // 依序解析所有 pin
      const pins: Pin[] = pinParts.map((pinPart, index) => {
        // 每 2 個數值一組
        const modeParts = chunk(pinPart, 2) as [number, number][];

        // 第一個數值為模式，第二個數值為解析度
        const capabilities: Capability[] = modeParts.map((modePart) => {
          const [mode, resolution] = modePart;
          return {
            mode, resolution
          }
        });

        return {
          number: index,
          capabilities,
        }
      });

      return { pins };
    },
  },

  // analog-mapping: 類比腳位映射表
  {
    eventName: 'analog-mapping',
    match(res) {
      const featureBytes = [0xF0, 0x6A];
      return matchFeature(res, featureBytes);
    },
    getData(values) {
      // 找到 6A 之 Index，從這裡開始往後找
      const index = values.findIndex((byte) => byte === 0x6A);

      const bytes = values.slice(index + 1, -1);

      const analogPinMap = bytes.reduce<AnalogPinMap>((map, byte, index) => {
        // 127（0x7F）表示不支援類比功能
        if (byte !== 127) {
          map[`${index}`] = byte;
        }

        return map;
      }, {});

      return { analogPinMap };
    },
  },

  // digital-message: 數位訊息回應
  {
    eventName: 'digital-data',
    match(res) {
      const hasCmd = res.some((byte) => byte >= 0x90 && byte <= 0x9F);
      return hasCmd;
    },
    getData(values) {
      // 取得所有特徵點位置
      const indexes = values.reduce<number[]>((acc, byte, index) => {
        if (byte >= 0x90 && byte <= 0x9F) {
          acc.push(index);
        }
        return acc;
      }, []);

      const responses = indexes.reduce<DigitalData[]>((acc, index) => {
        const bytes = values.slice(index + 1, index + 3);

        const target = values[index];
        if (!target) return acc;

        const port = target - 0x90;
        const value = significantBytesToNumber(bytes);

        acc.push({
          port, value,
        });
        return acc;
      }, []);

      return responses;
    },
  },

  // analog-message: 類比訊息回應
  {
    eventName: 'analog-data',
    match(res) {
      return res.some((byte) => byte >= 0xE0 && byte <= 0xEF);
    },
    getData(values) {
      // 取得所有特徵點位置
      const indexes = values.reduce<number[]>((acc, byte, index) => {
        if (byte >= 0xE0 && byte <= 0xEF) {
          acc.push(index);
        }
        return acc;
      }, []);

      const analogValues = indexes.reduce<AnalogData[]>((acc, index) => {
        const valueBytes = values.slice(index + 1, index + 3);

        const target = values[index];
        if (!target) return acc;

        const analogNumber = target - 0xE0;
        const value = significantBytesToNumber(valueBytes);

        acc.push({
          analogNumber, value,
        });
        return acc;
      }, []);

      return analogValues;
    },
  },

  // dht-message: DHT 感測器資料
  {
    eventName: 'dht-data',
    match(res) {
      // 回應開頭一定為 F0 74 00
      const featureBytes = [0xF0, 0x74, 0x00];
      return matchFeature(res, featureBytes);
    },
    getData(values) {
      const result = pipe(values,
        findLastIndex((value) => value === 0xF0),
        (index) => {
          const temperature = pipe(
            [values[index + 4], values[index + 5]],
            compact,
            significantBytesToNumber,
            divide(10),
          );

          const humidity = pipe(
            [values[index + 6], values[index + 7]],
            compact,
            significantBytesToNumber,
            divide(10),
          );

          return { temperature, humidity }
        },
      );

      return [result];
    },
  },


  // message: 
  {
    eventName: 'message',
    match: (res) => matchFeature(res, [0xF0, 0x71]),
    getData(bytes) {
      const text = pipe(bytes,
        /** 去除頭尾和 0 */
        reject((byte) => [0xF0, 0x71, 0xF7, 0x00].includes(byte)),
        map((byte) => String.fromCharCode(byte)),
        join(''),
      );

      return { text };
    },
  },
]