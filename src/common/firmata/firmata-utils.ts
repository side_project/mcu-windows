import { PinMode, PinModeKey, pinModeDefinitions } from '../../types';
import { CmdKey, cmds, FirmataCmdParams } from './cmd-define';
import { responses, EventName, FirmataData } from './response-define';

export interface ResponseParsedResult {
  eventName: `${EventName}`;
  oriBytes: number[];
  data: FirmataData;
}

/** 解析 Firmata 回傳資料 */
export function parseResponse(res: number[]): ResponseParsedResult[] {
  // 找出所有符合的回應
  const matchResDefines = responses.filter((define) =>
    define.match(res)
  );

  if (matchResDefines.length === 0) {
    return [];
  }

  const results = matchResDefines.map((resDefine) => {
    const data = resDefine.getData(res);
    const { eventName } = resDefine;

    const result: ResponseParsedResult = {
      eventName,
      oriBytes: res,
      data,
    }
    return result;
  });

  return results;
}

/** 取得 Firmata 命令資料，若命令不存在則會取得 undefined */
export function getCmdValue(key: `${CmdKey}`, params?: FirmataCmdParams) {
  const target = cmds.find((cmd) => cmd.key === key);
  return target?.getValue(params as never);
}

/** 取得腳位模式資訊
 * @param mode 腳位模式編號
 * @returns 
 */
export function getPinModeInfo(mode: number) {
  return pinModeDefinitions.find((item) => item.code === mode);
}
