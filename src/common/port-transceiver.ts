/* eslint-disable @typescript-eslint/no-unsafe-declaration-merging */
import EventEmitter2, { Listener, OnOptions } from 'eventemitter2';
import { debounce } from 'lodash-es';
import { CmdKey, FirmataCmdParams, FirmataCmd } from './firmata/cmd-define';
import { parseResponse, getCmdValue } from './firmata/firmata-utils';
import { FirmataInfo, EventName as ResponseEventName, FirmataPins, FirmataAnalogPinMap, DigitalData, AnalogData, DhtData, Message } from './firmata/response-define';
import { clone } from 'remeda';

export enum EventName {
  /** Port 成功開啟 */
  OPENED = 'opened',
  /** 發生異常 */
  ERROR = 'error',
  /** 收到未定義的回應 */
  UNDEFINED_RESPONSE = 'undefined-response',
}

export interface Options {
  /** 命令發送最小間距（ms） */
  writeInterval: number;
  /** Reader 完成讀取資料之 debounce 時間
   * 由於 Firmata 採樣頻率（Sampling Interval）預設是 19ms 一次
   * 所以只要設定小於 19ms 數值都行，考慮傳輸速度後，這裡取個整數，預設為 10ms
   * 
   * [參考文件 : Firmata Sampling Interval](https://github.com/firmata/protocol/blob/master/protocol.md#sampling-interval)
   */
  readEmitDebounce: number;
}

export interface DebounceFunctionMap {
  finishReceive?: ReturnType<typeof debounce>;
}

export interface CmdQueueItem {
  key: `${CmdKey}`;
  params: FirmataCmdParams;
  value: number[];
}

export class PortTransceiver extends EventEmitter2 {
  /** 透過 requestPort() 取得之目標 COM Port */
  private port?: SerialPort;
  private reader?: ReadableStreamDefaultReader<Uint8Array>;
  /** 暫存目前已接收的數值 */
  private receiveBuffer: number[] = [];

  private writer?: WritableStreamDefaultWriter<Uint8Array>;
  /** writer 計時器，用於持續發送命令 */
  private writeTimer?: ReturnType<typeof setInterval>;
  /** 命令佇列，用來儲存準備發送的命令 */
  private cmdsQueue: CmdQueueItem[] = [];

  /** 設定 */
  private options: Options = {
    writeInterval: 10,
    readEmitDebounce: 10,
  };

  /** debounce 原理與相關資料可以參考以下連結
   * 
   * [Debounce 和 Throttle](https://ithelp.ithome.com.tw/articles/10222749)
   */
  private debounce: DebounceFunctionMap = {
    finishReceive: undefined,
  };

  constructor(port: SerialPort) {
    super();

    this.port = port;

    this.debounce.finishReceive = debounce(() => {
      this.finishReceive();
    }, this.options.readEmitDebounce);
  }

  /** 開啟監聽 Port 資料 */
  async start() {
    if (!this?.port?.open) {
      return Promise.reject('Port 無法開啟');
    }

    try {
      // Firmata bps 固定為 57600
      await this.port.open({ baudRate: 115200 });
    } catch (error) {
      return Promise.reject(error);
    }

    this.emit(`${EventName.OPENED}`);
    this.startReader();
    this.startWriter();
  }
  /** 關閉 Port */
  stop() {
    this.removeAllListeners();

    clearInterval(this.writeTimer);
    this.writer?.releaseLock?.();

    this.reader?.releaseLock?.();
    this.port?.close?.();
  }

  /** Serial.Reader 開始讀取資料
   * 
   * 參考資料：
   * [W3C](https://wicg.github.io/serial/#readable-attribute)
   * [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Web_Serial_API#reading_data_from_a_port)
   */
  private async startReader() {
    const port = this.port;

    if (!port?.readable || port.readable.locked) return;

    try {
      this.reader = port.readable.getReader();

      for (; ;) {
        const { value, done } = await this.reader.read();
        if (done) break;

        // 將收到的資料儲存至 receiveBuffer
        this.receiveBuffer.push(...value);
        // 超過設定時間之後，呼叫 finishReceive()
        this.debounce.finishReceive?.();
      }
    } catch (err) {
      // 對外發出錯誤訊息
      this.emit(`${EventName.ERROR}`, err);
    } finally {
      this.reader?.releaseLock();
    }
  }
  /** 完成接收，emit 已接收資料 */
  private finishReceive() {
    // console.log(`🚀 ~ [finishReceive] receiveBuffer: `, this.receiveBuffer);

    // 解析回應內容
    const results = parseResponse(this.receiveBuffer);
    // console.log(`🚀 ~ [finishReceive] results: `, clone(results));

    if (results.length === 0) {
      this.emit(`${EventName.UNDEFINED_RESPONSE}`, [...this.receiveBuffer]);
      this.receiveBuffer.length = 0;
      return;
    }

    // emit 所有解析結果 
    results.forEach(({ eventName, data }) => {
      this.emit(eventName, data);
    });


    // 清空 buffer
    this.receiveBuffer.length = 0;
  }

  /** 加入發送命令 */
  async addCmd(key: `${CmdKey}`, params?: FirmataCmdParams) {
    const cmdValue = getCmdValue(key, params);
    if (!cmdValue) {
      return Promise.reject(`${key} 命令不存在`);
    }

    const item: CmdQueueItem = {
      key: key,
      params,
      value: cmdValue,
    }

    this.cmdsQueue.push(item);
    return item;
  }
  /** 取得 Serial.Writer 並開啟發送佇列
   * 
   * 參考資料：
   * [W3C](https://wicg.github.io/serial/#writable-attribute)
   */
  private startWriter() {
    this.writeTimer = setInterval(() => {
      const port = this.port;

      if (!port?.writable || this.cmdsQueue.length === 0) {
        return;
      }

      this.writer = port.writable.getWriter();

      const cmd = this.cmdsQueue.shift();
      if (!cmd) return;

      this.write(cmd?.value);
    }, this.options.writeInterval);
  }
  /** 透過 Serial.Writer 發送資料 */
  private async write(data: number[]) {
    if (!this.writer) {
      return Promise.reject('writer 不存在');
    }

    await this.writer.write(new Uint8Array(data));
    this.writer.releaseLock();
  }
}

type ListenerOption = boolean | OnOptions;

export interface PortTransceiver {
  on(event: `${EventName.OPENED}`, listener: () => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.ERROR}`, listener: (error: unknown) => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.UNDEFINED_RESPONSE}`, listener: (res: number[]) => void, options?: ListenerOption): this | Listener;

  on(event: `${ResponseEventName.MESSAGE}`, listener: (data: Message) => void, options?: ListenerOption): this | Listener;
  on(event: `${ResponseEventName.FIRMWARE_NAME}`, listener: (data: FirmataInfo) => void, options?: ListenerOption): this | Listener;
  on(event: `${ResponseEventName.CAPABILITY}`, listener: (data: FirmataPins) => void, options?: ListenerOption): this | Listener;
  on(event: `${ResponseEventName.ANALOG_MAPPING}`, listener: (data: FirmataAnalogPinMap) => void, options?: ListenerOption): this | Listener;
  on(event: `${ResponseEventName.DIGITAL_DATA}`, listener: (data: DigitalData[]) => void, options?: ListenerOption): this | Listener;
  on(event: `${ResponseEventName.ANALOG_DATA}`, listener: (data: AnalogData[]) => void, options?: ListenerOption): this | Listener;
  on(event: `${ResponseEventName.DHT_DATA}`, listener: (data: DhtData[]) => void, options?: ListenerOption): this | Listener;

  once(event: `${EventName.OPENED}`, listener: () => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.ERROR}`, listener: (error: unknown) => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.UNDEFINED_RESPONSE}`, listener: (res: number[]) => void, options?: ListenerOption): this | Listener;

  once(event: `${ResponseEventName.MESSAGE}`, listener: (data: Message) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.FIRMWARE_NAME}`, listener: (data: FirmataInfo) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.CAPABILITY}`, listener: (data: FirmataPins) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.ANALOG_MAPPING}`, listener: (data: FirmataAnalogPinMap) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.DIGITAL_DATA}`, listener: (data: DigitalData[]) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.DIGITAL_DATA}`, listener: (data: DigitalData[]) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.ANALOG_DATA}`, listener: (data: AnalogData[]) => void, options?: ListenerOption): this | Listener;
  once(event: `${ResponseEventName.DHT_DATA}`, listener: (data: DhtData[]) => void, options?: ListenerOption): this | Listener;

}
