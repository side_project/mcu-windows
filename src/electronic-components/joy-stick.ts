/* eslint-disable @typescript-eslint/no-unsafe-declaration-merging */
import EventEmitter2, { Listener, OnOptions } from "eventemitter2";
import { defaultsDeep, findLast, throttle } from "lodash-es";
import { AnalogData, AnalogPinMap, Pin } from "../common/firmata/response-define";
import { PortTransceiver } from "../common/port-transceiver";
import { Button } from "./button";
import { PinMode } from "../types";

/** 軸向設定值 */
export interface AxisOption {
  /** 訊號來源腳位 */
  pin: Pin;
  /** 原點。搖桿無動作時，類比數值基準點 */
  origin?: number;
  /** 閥值。origin 正負 threshold 之內的數值，視為沒有動作，以免產生大量雜訊。 */
  threshold?: number;
  /** 軸反轉，可以讓搖桿因應多種使用方式 */
  isReverse?: boolean;
}

export interface ConstructorParams {
  transceiver: PortTransceiver;
  /** 類比腳位映射表 */
  analogPinMap: AnalogPinMap;
  /** X 軸設定 */
  xAxis: AxisOption;
  /** Y 軸設定 */
  yAxis: AxisOption;
  /** 按鈕設定 */
  btn: {
    /** 訊號來源腳位 */
    pin: Pin;
    /** 按鈕腳位模式 */
    mode: PinMode;
  }
}

export enum EventName {
  /** 按鈕事件 */
  RISING = 'rising',
  FALLING = 'falling',
  TOGGLE = 'toggle',
  /** 搖桿動作 */
  DATA = 'data',
}

const axisOptionDefault: Omit<AxisOption, 'pin'> = {
  origin: 510,
  threshold: 20,
  isReverse: false,
}

/** 
 * 搖桿
 * 
 * 繼承 EventEmitter2 功能，支援 XY 類比訊號與 1 個按鈕數位訊號
 */
export class JoyStick extends EventEmitter2 {
  /** X 軸設定 */
  private xAxis: Required<AxisOption>;
  /** Y 軸設定 */
  private yAxis: Required<AxisOption>;
  /** 按鈕物件 */
  private btn: Button;
  /** 類比腳位映射表 */
  analogPinMap: AnalogPinMap;
  /** COM Port 收發器 */
  private portTransceiver: PortTransceiver;
  /** 訊號回報監聽器 */
  private listener?: Listener;

  /** 建立 throttle 功能 */
  throttle: {
    setValue: ReturnType<typeof throttle>
  }

  /** 目前軸類比數值 */
  axesValue = {
    x: 0,
    y: 0,
  };

  constructor(params: ConstructorParams) {
    super();

    const {
      transceiver,
      analogPinMap,
      xAxis,
      yAxis,
      btn,
    } = params;

    if (!transceiver) throw new Error(`transceiver 必填`);
    if (!analogPinMap) throw new Error(`analogPinMap 必填`);
    if (!xAxis?.pin) throw new Error(`xAxis.pin 必填`);
    if (!yAxis?.pin) throw new Error(`yAxis.pin 必填`);
    if (!btn?.pin) throw new Error(`btn.pin 必填`);

    this.xAxis = defaultsDeep(xAxis, axisOptionDefault);
    this.yAxis = defaultsDeep(yAxis, axisOptionDefault);
    this.portTransceiver = transceiver;
    this.analogPinMap = analogPinMap;

    this.btn = new Button({
      ...btn,
      transceiver,
    });
    /** 將所有 btn 事件轉送出去 */
    this.btn.onAny((event, value) => this.emit(event, value));

    /** 建立 throttle 功能 */
    this.throttle = {
      setValue: throttle((x: number, y: number) => {
        this.setValue(x, y);
      }, 100),
    }

    this.init();
  }

  async init() {
    const xPinNum = this.xAxis.pin.number;
    const yPinNum = this.yAxis.pin.number;

    this.portTransceiver.addCmd('set-mode', {
      pin: xPinNum,
      mode: PinMode.ANALOG_INPUT,
    });
    this.portTransceiver.addCmd('set-mode', {
      pin: yPinNum,
      mode: PinMode.ANALOG_INPUT,
    });

    this.listener = this.portTransceiver.on('analog-data', (data) => {
      this.handleData(data);
    }, { objectify: true }) as Listener;
  }
  destroy() {
    this.btn.destroy();
    this.listener?.off();
    this.removeAllListeners();
  }

  private handleData(data: AnalogData[]) {
    const { xAxis, yAxis, analogPinMap } = this;

    let x = 0;
    let y = 0;

    const xAnalogNumber = analogPinMap[xAxis.pin.number];
    const yAnalogNumber = analogPinMap[yAxis.pin.number];

    /** 從腳位編號映射成 analog 編號後，取得 X、Y 軸類比資料 */
    const [xAnalogData, yAnalogData] = [xAnalogNumber, yAnalogNumber].map((mapNumber) => {
      return findLast(data, ({ analogNumber }) => mapNumber === analogNumber);
    })

    if (xAnalogData) {
      x = this.getAxisValue(xAnalogData.value, xAxis)
    }
    if (yAnalogData) {
      y = this.getAxisValue(yAnalogData.value, yAxis)
    }

    this.throttle.setValue(x, y);
  }

  private setValue(x: number, y: number) {
    this.axesValue.x = x;
    this.axesValue.y = y;

    this.emit('data', this.axesValue);
  }

  /** 將類比數值轉換為搖桿軸資料
   * @param value 類比訊號原始數值
   * @param options 軸向設定值
   * @returns 
   */
  private getAxisValue(value: number, options: Required<AxisOption>) {
    const { origin, threshold, isReverse } = options;

    /** 
     * 需要設定 threshold，因為實際上搖桿回到中點時，
     * 類比數值並非完全靜止，可能會在正負 1 或 2 些微浮動，
     * 如果判斷搖桿靜止是用單一數值判斷，容易誤判，
     * 所以透過 threshold，以範圍來判斷，就可以解決誤判問題。
     */
    const delta = origin - value;
    if (Math.abs(delta) < threshold) {
      return 0;
    }

    return isReverse ? delta * -1 : delta;
  }
}

type ListenerOption = boolean | OnOptions;
export interface JoyStick {
  on(event: `${EventName.RISING}`, listener: () => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.FALLING}`, listener: () => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.TOGGLE}`, listener: (value: boolean) => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.DATA}`, listener: (data: { x: number, y: number }) => void, options?: ListenerOption): this | Listener;

  once(event: `${EventName.RISING}`, listener: () => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.FALLING}`, listener: () => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.TOGGLE}`, listener: (value: boolean) => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.DATA}`, listener: (data: { x: number, y: number }) => void, options?: ListenerOption): this | Listener;
}