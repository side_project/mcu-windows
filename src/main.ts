import { createApp } from 'vue'
import { createPinia } from 'pinia'

import { Quasar, Dialog, Notify } from 'quasar'
import quasarLang from 'quasar/lang/zh-TW'

// Import icon libraries
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-symbols-rounded/material-symbols-rounded.css'
import iconSet from 'quasar/icon-set/material-symbols-rounded'

// Import Quasar css
import 'quasar/src/css/index.sass'
import App from './App.vue'

import VueGtag from 'vue-gtag';
import { createGtm } from '@gtm-support/vue-gtm';

// 自訂樣式
import './style/global.sass'
import './style/animate.sass'

// import './index.css'
import 'virtual:uno.css'

createApp(App)
  .use(
    createGtm({
      id: 'GTM-P3SBNQJ9',
    })
  )
  .use(VueGtag, {
    config: { id: 'G-CGSNXM95RJ' }
  })
  .use(Quasar, {
    plugins: {
      Dialog, Notify
    },
    lang: quasarLang,
    iconSet,
  })
  .use(createPinia())
  .mount('#app')

