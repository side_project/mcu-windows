import { nanoid } from 'nanoid';
import { acceptHMRUpdate, defineStore } from 'pinia';
import { computed, defineComponent, markRaw, ref } from 'vue';
import { Pin } from '../common/firmata/response-define';
import { WindowName, Window } from '../types';

import DigitalIoWindow from '../components/digital-io-window.vue';
import AnalogInputWindow from '../components/analog-input-window.vue';
import PwmOutputWindow from '../components/pwm-output-window.vue';

import RgbLedPaletteWindow from '../components/rgb-led-palette-window.vue';
import MorseCodeTransmitterWindow from '../components/morse-code-transmitter-window.vue';
import ThermoHygrometerWindow from '../components/thermo-hygrometer-window.vue';

import GoogleDinoWindow from '../components/google-dino-window.vue';
import CatVsDogWindow from '../components/cat-vs-dog-window.vue';

import { useTrack } from '../composables/use-track';

/** 列舉並儲存視窗元件 */
const windowComponentMap: Record<WindowName, ReturnType<typeof defineComponent>> = {
  'digital-io': markRaw(DigitalIoWindow),
  'analog-input': markRaw(AnalogInputWindow),
  'pwm-output': markRaw(PwmOutputWindow),

  'rgb-led-palette': markRaw(RgbLedPaletteWindow),
  'morse-code-transmitter': markRaw(MorseCodeTransmitterWindow),
  'thermo-hygrometer': markRaw(ThermoHygrometerWindow),

  'google-dino': markRaw(GoogleDinoWindow),
  'cat-vs-dog': markRaw(CatVsDogWindow),
}

export const useWindowStore = defineStore('window', () => {
  const map = ref<Map<string, Window>>(new Map());
  const focusedId = ref('');

  const { event } = useTrack();

  function add(name: WindowName) {
    /** 使用 nanoid 生成 ID */
    const id = nanoid();

    map.value.set(id, {
      component: windowComponentMap[name],
      name,
      id,
      focusAt: Date.now(),
      occupiedPins: [],
    });

    event({
      event: 'add_window',
      category: 'window',
      label: name,
    });
  }

  function remove(id: string) {
    const target = map.value.get(id);

    map.value.delete(id);

    if (target) {
      event({
        event: 'remove_window',
        category: 'window',
        label: target.name,
      });
    }
  }

  async function setFocus(id = '') {
    focusedId.value = id;
    if (!id) return;

    const target = map.value.get(id);
    if (!target) {
      return Promise.reject(`視窗不存在`);
    }

    target.focusAt = Date.now();
  }

  async function addOccupiedPin(id: string, pin: Pin) {
    const windows = [...map.value.values()];

    const target = windows.find((window) => window.id === id);
    if (!target) {
      return Promise.reject(`window 不存在`);
    }

    target.occupiedPins.push(pin);

    event({
      event: 'reserve_pin',
      category: 'pin',
      label: `${pin.number}`,
    });
  }

  async function deleteOccupiedPin(id: string, pin: Pin) {
    const windows = [...map.value.values()];

    const target = windows.find((window) => window.id === id);
    if (!target) {
      return Promise.reject(`window 不存在`);
    }

    const targetPinIndex = target.occupiedPins.findIndex(({ number }) =>
      number === pin.number
    );
    if (targetPinIndex < 0) return;

    target.occupiedPins.splice(targetPinIndex, 1);

    event({
      event: 'unreserve_pin',
      category: 'pin',
      label: `${pin.number}`,
    });
  }

  const zIndexMap = computed(() => {
    const windows = [...map.value].map(([key, value]) => value);

    windows.sort((a, b) => a.focusAt > b.focusAt ? 1 : -1);

    return windows.reduce((map, window, index) =>
      map.set(window.id, index),
      new Map<string, number>()
    );
  });

  const occupiedPins = computed(() => {
    const windows = [...map.value.values()];

    // 找出有占用腳位的 window
    const occupiedPinWindows = windows.filter(({ occupiedPins }) =>
      occupiedPins.length !== 0
    );

    const occupiedPins = occupiedPinWindows.reduce<{
      pin: Pin;
      ownedWindowId: string;
    }[]>((acc, window) => {
      window.occupiedPins.forEach((pin) => {
        acc.push({
          pin,
          ownedWindowId: window.id,
        });
      });

      return acc;
    }, []);

    return occupiedPins;
  });

  return {
    /** 視窗 ID Map */
    map,
    focusedId,

    /** 新增視窗 */
    add,
    /** 刪除視窗 */
    remove,
    /** focus 指定視窗 */
    setFocus,
    /** 新增 Window 占用腳位 */
    addOccupiedPin,
    /** 移除 Window 占用腳位 */
    deleteOccupiedPin,

    /** 視窗 ID 對應 z-index */
    zIndexMap,
    /** 所有被占用腳位，包含占用 window 的 ID */
    occupiedPins,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useWindowStore, import.meta.hot))
}
