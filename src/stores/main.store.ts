import { useBroadcastChannel, watchOnce } from '@vueuse/core';
import { defineStore, acceptHMRUpdate } from 'pinia';
import { ref, watch } from 'vue';
import { usePortStore } from './port.store';
import { openBaseDialog } from '../common/utils-quasar';
import { useQuasar } from 'quasar';

export const useMainStore = defineStore('main', () => {
  const portStore = usePortStore();
  const $q = useQuasar();

  const {
    post, data
  } = useBroadcastChannel({ name: 'mcu-windows' })

  watchOnce(data, () => {
    openBaseDialog({
      icon: {
        name: 'gpp_bad',
        color: 'negative',
      },
      message: '偵測到重複開啟此網頁，為了防止 Serial COM 占用，此網頁已自動停用',
      actions: [],
      persistent: true,
    });

    portStore.transceiver?.stop();
    portStore.port?.close();
  })

  return {
    /** 檢查是否多開 */
    checkSingletonWeb() {
      post('mcu-windows');
    }
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useMainStore, import.meta.hot))
}