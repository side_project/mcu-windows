import { acceptHMRUpdate, defineStore } from 'pinia';
import { PortTransceiver } from '../common/port-transceiver';
import { shallowRef } from 'vue';

interface State {
  port?: SerialPort;
  transceiver?: PortTransceiver;
}

export const usePortStore = defineStore('port', () => {
  const port = shallowRef<SerialPort>();
  const transceiver = shallowRef<PortTransceiver>();

  /** 設定 Port，將 SerialPort 物件儲存至 store 中 */
  function setPort(data: SerialPort) {
    port.value = data;

    // 若 transceiver 已經建立，則關閉
    transceiver.value?.stop();

    // 建立 PortTransceiver 物件
    transceiver.value = new PortTransceiver(data);
    transceiver.value.start();
  }

  return {
    port,
    transceiver,

    setPort,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(usePortStore, import.meta.hot))
}
