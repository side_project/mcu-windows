import { acceptHMRUpdate, defineStore } from 'pinia';
import { AnalogPinMap, FirmataInfo, Pin } from '../common/firmata/response-define';
import { ref } from 'vue';
import { useTrack } from '../composables/use-track';

interface BoardInfo {
  version?: string;
  firmwareName?: string;
  pins: Pin[];
  analogPinMap?: AnalogPinMap;
}

export const useBoardStore = defineStore('board', () => {
  const version = ref('');
  const firmwareName = ref('');
  const pins = ref<Pin[]>([]);
  const analogPinMap = ref<AnalogPinMap>();

  const { event } = useTrack();

  function setInfo(info: FirmataInfo) {
    version.value = info.version;
    firmwareName.value = info.firmwareName;

    event({
      event: 'get_firmware_info',
      category: 'firmware',
      label: info.firmwareName,
    })

    event({
      event: 'get_firmware_info',
      category: 'version',
      label: info.version,
    })
  }

  function setPins(data: Pin[]) {
    pins.value = data;
  }

  function setAnalogPinMap(data: AnalogPinMap) {
    analogPinMap.value = data;
  }

  return {
    version,
    firmwareName,
    pins,
    analogPinMap,

    setInfo,
    setPins,
    setAnalogPinMap,
  }
})


if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useBoardStore, import.meta.hot))
}
