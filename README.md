# 歡迎來到 MCU Windows！

[網站連結](https://mcu-windows-side-project-ef3dad6121bc7b6784536acfc697102aa0782d.gitlab.io)

此網頁為 Web Serial API 的實驗項目之一，目的是讓微控制器（MCU）與網頁連上線，讓真實的電子訊號在網頁上呈現豐富有趣的互動，並延伸更多有趣的應用。

可以如同電腦桌面一般開啟多個視窗，每個視窗有其特殊功能，實際範例可以看看以下影片。

# 相關資源

本專案推薦配合 Arduino Uno 使用，沒有零件的話可以[來這裡看看](https://www.ruten.com.tw/item/show?22245679885806)。

實際範例可以看看此[影片](https://www.youtube.com/watch?v=OpayalfQ124)。

此專案延伸從鐵人賽參賽內容延伸，想看[鐵人賽系列文章點此](https://ithelp.ithome.com.tw/users/20140213/ironman/4765)。

感謝博碩文化，此專案有[彙整成冊](https://www.books.com.tw/products/0010942459)，書中有更詳細的說明與解釋，並使用 TypeScript 與 Vue 3 重構，歡迎大家買來看看。

關於我可以[點這裡](https://www.linkedin.com/in/shih-chen-lin-codfish)。